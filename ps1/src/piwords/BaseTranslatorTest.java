package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        // Expect that .01 in base-2 is .25 in base-10
        // (0 * 1/2^1 + 1 * 1/2^2 = .25)
    	int[] inputA = {0, 1};
        int[] expectedOutputA = {2, 5};
        assertArrayEquals(expectedOutputA,
                          BaseTranslator.convertBase(inputA, 2, 10, 2));
        
        int[] inputB = {10, 11, 12, 13};
        int[] expectedOutB = {1, 0, 1, 0};
        assertArrayEquals(expectedOutB,
                          BaseTranslator.convertBase(inputB, 16, 2, 4)); 
        
        int[] inputC = {6, 7, 8};
        int[] expectedOutC = {10, 13, 9};
        assertArrayEquals(expectedOutC,
                          BaseTranslator.convertBase(inputC, 10, 16, 3));
        // baseA < 2
        int[] inputD = {0, 0};
        int[] expectedOutD = null;
        assertArrayEquals(expectedOutD,
                          BaseTranslator.convertBase(inputD, 1, 16, 2)); 
        // baseB < 2
        int[] inputE = {4, 7, 2, 5, 7};
        int[] expectedOutE = null;
        assertArrayEquals(expectedOutE,
                          BaseTranslator.convertBase(inputE, 10, 1, 5)); 
        // precisionB < 1
        int[] inputF = {1, 2, 3, 4, 5, 6};
        int[] expectedOutF = null;
        assertArrayEquals(expectedOutF,
                          BaseTranslator.convertBase(inputF, 10, 2, 0)); 
    }
}
    
