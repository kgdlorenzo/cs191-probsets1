package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
	
    @Test
    public void basicPowerModTest() {
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(6, PiGenerator.powerMod(6, 2, 10));
        assertEquals(1, PiGenerator.powerMod(5, 0, 18));
        assertEquals(0, PiGenerator.powerMod(5, 2, 5));
        assertEquals(0, PiGenerator.powerMod(6, 5, 1));
        assertEquals(0, PiGenerator.powerMod(0, 10, 20));
    }
    
    @Test
    public void computePiInHexTest() {
    	int[] answer = { 3, 2, 4, 3, 15, 6, 10 };
    	assertArrayEquals(answer, PiGenerator.computePiInHex(7));
    }

    // TODO: Write more tests (Problem 1.a, 1.c)
}
