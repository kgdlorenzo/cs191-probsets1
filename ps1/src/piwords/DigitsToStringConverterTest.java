package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        // Input is a 4 digit number, 0.123 represented in base 4
        int[] inputA = {0, 1, 2, 3};
        // Want to map 0 -> "d", 1 -> "c", 2 -> "b", 3 -> "a"
        char[] alphabetA = {'d', 'c', 'b', 'a'};

        String expectedOutputA = "dcba";
        assertEquals(expectedOutputA,
                     DigitsToStringConverter.convertDigitsToString(
                             inputA, 4, alphabetA));
       
        int[] inputB = {1, 3, 0, 2};
        char[] alphabetB = {'u', 'p', 'l', 'a'};
        String expectedOutputB = "paul";
        assertEquals(expectedOutputB,
                     DigitsToStringConverter.convertDigitsToString(
                             inputB, 4, alphabetB));
        
        int[] inputC = {5, 4, 3, 2, 1};
        char[] alphabetC = {'s', 'a', 's', 'y'};
        String expectedOutputC = null;
        assertEquals(expectedOutputC,
                     DigitsToStringConverter.convertDigitsToString(
                             inputC, 6, alphabetC));
    }
}